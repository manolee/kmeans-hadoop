package ch.epfl.advdb.keys;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

/**
 * Key used during matching of clusters.
 * Composite key used to impose ordering as values show at reducer
 * Aim: First cluster info for IMDB arrives.
 * Then, all movies from Netflix show up, ordered by the cluster in which they belong
 *  
 * @author manolee
 *
 */
public class ClusterPair  implements WritableComparable {

	int clusterIMDB;
	int clusterNetflix; // will set to -1 if I am dealing with a value from IMDB
	
	
	public ClusterPair(int clusterIMDB, int clusterNetflix) {
		super();
		this.clusterIMDB = clusterIMDB;
		this.clusterNetflix = clusterNetflix;
	}

	public ClusterPair() {
		super();
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		clusterIMDB = in.readInt();
		clusterNetflix = in.readInt();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(clusterIMDB);
		out.writeInt(clusterNetflix);
	}

	
	
	public int getClusterIMDB() {
		return clusterIMDB;
	}

	public void setClusterIMDB(int clusterIMDB) {
		this.clusterIMDB = clusterIMDB;
	}

	public int getClusterNetflix() {
		return clusterNetflix;
	}

	public void setClusterNetflix(int clusterNetflix) {
		this.clusterNetflix = clusterNetflix;
	}

	//Used/extended by comparators
	@Override
	public int compareTo(Object other) {
		ClusterPair o = (ClusterPair)other;
		if (this.getClusterIMDB() < o.getClusterIMDB()) {
			return -1;
		} else if (this.getClusterIMDB() > o.getClusterIMDB()) {
			return +1;
		}
		return 0;
	}

	public static int compare(ClusterPair ip1, ClusterPair ip2) {

		if (ip1.getClusterIMDB() < ip2.getClusterIMDB()) {
			return -1;
		} else if (ip1.getClusterIMDB() > ip2.getClusterIMDB()) {
			return +1;
		}


		return 0;
	}

	@Override
	public String toString() {
		return "ClusterPair [clusterIMDB=" + clusterIMDB + ", clusterNetflix="
				+ clusterNetflix + "]";
	} 

	

}

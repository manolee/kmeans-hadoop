package ch.epfl.advdb.keys;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.WritableComparable;

/**
 * Key used when test movies fed in system
 * Index indicates reducer (values : 0 - netflixK) 
 * 
 *  
 * @author manolee
 *
 */
public class TestsKey  implements WritableComparable {

	int index;
	int type; //Used to differentiate between the cluster's V-like entries and actual U entries
	//0: cluster entries -- 1: U entries
	
	
	public TestsKey(int index, int type) {
		super();
		this.index = index;
		this.type = type;
	}

	public TestsKey() {
		super();
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		index = in.readInt();
		type = in.readInt();
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(index);
		out.writeInt(type);
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	//Used/extended by comparators
	@Override
	public int compareTo(Object other) {
		TestsKey o = (TestsKey)other;
		if (this.getIndex() < o.getIndex()) {
			return -1;
		} else if (this.getIndex() > o.getIndex()) {
			return +1;
		}
		return 0;
	}

	public static int compare(TestsKey ip1, TestsKey ip2) {

		if (ip1.getIndex() < ip2.getIndex()) {
			return -1;
		} else if (ip1.getIndex() > ip2.getIndex()) {
			return +1;
		}


		return 0;
	}

	@Override
	public String toString() {
		return "TestsKey [index=" + index + ", type=" + type + "]";
	}


}

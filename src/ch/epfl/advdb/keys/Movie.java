package ch.epfl.advdb.keys;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

/**
 * 
 * @author manolee
 * Not used in delivered version
 * Used when computing F-Score, to differentiate between utility matrix and my calculations
 */

public class Movie implements WritableComparable
{
	int mid;
	char matrixType; //M: actual info - X: our results

	public Movie() {

	}

	public Movie(int mid, char matrixType) {
		super();
		this.mid = mid;
		this.matrixType = matrixType;
	}

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public char getMatrixType() {
		return matrixType;
	}

	public void setMatrixType(char matrixType) {
		this.matrixType = matrixType;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		mid = in.readInt();
		matrixType = in.readChar();

	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(mid);
		out.writeChar(matrixType);
	}

	@Override
	public String toString() {
		return "Movie [mid=" + mid + ", matrixType=" + matrixType + "]";
	}

	@Override
	public int compareTo(Object other) {
		if(this.mid < ((Movie)other).mid)
		{
			return -1;
		}
		else if(this.mid > ((Movie)other).mid)
		{
			return 1;
		}
		return 0;
	}



	public static int compare(Movie ip1, Movie ip2) {

		if (ip1.mid < ip2.mid) {
			return -1;
		} else if (ip1.mid > ip2.mid) {
			return +1;
		}


		return 0;
	}



}
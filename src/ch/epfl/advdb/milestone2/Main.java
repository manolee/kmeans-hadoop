package ch.epfl.advdb.milestone2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

import javax.sound.midi.SysexMessage;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

import ch.epfl.advdb.keys.ClusterPair;
import ch.epfl.advdb.keys.Movie;
import ch.epfl.advdb.keys.TestsKey;
import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.util.Constants.KMeansCounter;
import ch.epfl.advdb.util.KMeansComparators;
import ch.epfl.advdb.util.KMeansComparators.ClusterPairComparator;
import ch.epfl.advdb.util.KMeansComparators.ClusterPairGroupComparator;
import ch.epfl.advdb.util.KMeansComparators.ClusterPairPartitioner;
import ch.epfl.advdb.util.KMeansComparators.MoviesGroupComparator;
import ch.epfl.advdb.util.KMeansComparators.MoviesPartitioner;
import ch.epfl.advdb.util.KMeansComparators.TestsKeyComparator;
import ch.epfl.advdb.util.KMeansComparators.TestsKeyGroupComparator;
import ch.epfl.advdb.util.KMeansComparators.TestsKeyPartitioner;
import ch.epfl.advdb.util.KMeansComparators.VAggregationPartitioner;
import ch.epfl.advdb.values.Feature;
import ch.epfl.advdb.values.IMDBCentroid;
import ch.epfl.advdb.values.IMDBFeatureVector;
import ch.epfl.advdb.values.NetflixFeatureVector;
import ch.epfl.advdb.values.UVMField;

public class Main {

	public static void main(String[] args) throws IOException, URISyntaxException, ClassNotFoundException, InterruptedException
	{
		/**
		 * Arguments and way of calling prog
		 */
		if (args.length != 3) {
			System.err.println("Usage: milestone2.jar <train> <test> <out>");
			System.exit(2);
		}

		Configuration conf = new Configuration();
		FileSystem fs = FileSystem.get(conf);


		conf.set("mapred.textoutputformat.separator", ",");
		conf.setInt("mapred.reduce.tasks", Constants.reducersNo);


		//Cluster execution
		int remoteExec = 1;


		String homePath = "";
		if(remoteExec == 0)
		{
			homePath = "/home/manolee/";
		}
		else
		{
			homePath = "/std69/";
		}

		//Logging = not used in remote execution
		PrintStream ps;
		if(remoteExec == 0)
		{
			ps = new PrintStream(homePath+"hadoopStuff/logs/out.log");
			System.setOut(ps);
		}


		//Paths used during program execution
		Path inDirIMDB = new Path(args[0]+"/features");
		Path inDirV = new Path(args[0]+"/V");
		Path inDirU = new Path(args[0]+"/U");


		Path testMoviesDir = new Path(args[1]+"/features");
		Path inDirM = new Path(args[1]+"/ratings");

		Path outDir = new Path(args[2]+"/");
		Path tmpDir = new Path(args[2]+"/../tmp/");
		Path imdbCentroidsDir = new Path(args[2]+"/../tmp/imdbCentroids_0/");
		Path netflixCentroidsDir = new Path(args[2]+"/../tmp/netflixCentroids_0/");
		Path curatedV = new Path(args[2]+"/../tmp/V");
		Path curatedU = new Path(args[2]+"/../tmp/U");
		Path imdbClustersInjection = imdbCentroidsDir;  
		Path netflixClustersInjection = netflixCentroidsDir; 

		Path matchingOut = new Path(args[2]+"/../tmp/clusterMatch");

		Path curatedM = new Path(args[2]+"/../tmp/M");

		Path scores = new Path(args[2]+"/../tmp/scores");
		Path userSuggestions = new Path(args[2]);


		/**
		 * Cleanup prior to new execution
		 */
		if (fs.exists(outDir)) {
			fs.delete(outDir, true);
		}

		if (fs.exists(tmpDir)) {
			fs.delete(tmpDir, true);
		}
		fs.mkdirs(tmpDir);

		//Only local use
		if(remoteExec == 0)
		{
			if (fs.exists(inDirIMDB)) {
				fs.delete(inDirIMDB, true);
			}
			fs.mkdirs(inDirIMDB);	



			if (fs.exists(inDirV)) {
				fs.delete(inDirV, true);
			}
			fs.mkdirs(inDirV);	

			if (fs.exists(inDirU)) {
				fs.delete(inDirU, true);
			}
			fs.mkdirs(inDirU);

			if (fs.exists(inDirM)) {
				fs.delete(inDirM, true);
			}
			fs.mkdirs(inDirM);

			if (fs.exists(testMoviesDir)) {
				fs.delete(testMoviesDir, true);
			}
			fs.mkdirs(testMoviesDir);

			if (fs.exists(inDirM)) {
				fs.delete(inDirM, true);
			}
			fs.mkdirs(inDirM);

			File[] filesIMDB = new File("/home/manolee/hadoopStuff/imdb-small/train/features/").listFiles();
			for(File file : filesIMDB)
			{
				fs.copyFromLocalFile(false, true, new Path(file.getAbsolutePath()),inDirIMDB);
			}
			/**
			 * Path for V
			 */
			File[] filesNetflix = new File("/home/manolee/hadoopStuff/imdb-small/train/V/").listFiles();
			for(File file : filesNetflix)
			{
				fs.copyFromLocalFile(false, true, new Path(file.getAbsolutePath()),inDirV);
			}
			/**
			 * Path for Utility Matrix M
			 */
			File[] filesM = new File("/home/manolee/hadoopStuff/imdb-small/test/ratings").listFiles();
			for(File file : filesM)
			{
				fs.copyFromLocalFile(false, true, new Path(file.getAbsolutePath()),inDirM);
			}
			/**
			 * Path for U
			 */
			File[] filesU = new File("/home/manolee/hadoopStuff/imdb-small/train/U/").listFiles();
			for(File file : filesU)
			{
				fs.copyFromLocalFile(false, true, new Path(file.getAbsolutePath()),inDirU);
			}

			/**
			 * Test Movies
			 */
			fs.copyFromLocalFile(false, true, new Path(homePath+"/hadoopStuff/imdb-small/test/features/features_test.dat"),testMoviesDir);


			//Debugging
			fs.copyFromLocalFile(false, true, new Path(homePath+"hadoopStuff/data/input/imdbCentroids_0"),imdbCentroidsDir);
			fs.copyFromLocalFile(false, true, new Path(homePath+"hadoopStuff/data/input/netflixCentroids_0"),netflixCentroidsDir);

		}
		//				//Debugging
		//				else
		//				{
		//					fs.copyFromLocalFile(false, true, new Path("/export/home/std69/deleteme/imdbCentroids_0"),imdbCentroidsDir);
		//					fs.copyFromLocalFile(false, true, new Path("/export/home/std69/deleteme/netflixCentroids_0"),netflixCentroidsDir);
		//				}


		/**
		 * ACTUAL EXECUTION
		 */

		System.out.println("Aggregating V");
		/**
		 * NETFLIX
		 */
		//Must first 'normalize' V - Store it in rows + without garbage

		Job aggregationV = new Job(conf, "aggregating values of V");

		aggregationV.setJarByClass(Main.class);
		aggregationV.setMapperClass(ch.epfl.advdb.mappers.VAggregatorMapper.class);
		aggregationV.setReducerClass(ch.epfl.advdb.reducers.VAggregatorReducer.class);
		aggregationV.setOutputKeyClass(Text.class);
		aggregationV.setOutputValueClass(UVMField.class);
		aggregationV.setPartitionerClass(VAggregationPartitioner.class);

		FileInputFormat.addInputPath(aggregationV, inDirV);

		FileOutputFormat.setOutputPath(aggregationV, curatedV);
		aggregationV.waitForCompletion(true);

		System.out.println("Aggregating U");
		//Should also 'normalize' U - will be needed later on

		//conf.setInt("mapred.reduce.tasks", Constants.reducersNo - 1);
		Job aggregationU = new Job(conf, "aggregating values of U");
		aggregationU.setJarByClass(Main.class);
		aggregationU.setMapperClass(ch.epfl.advdb.mappers.UAggregatorMapper.class);
		aggregationU.setReducerClass(ch.epfl.advdb.reducers.UAggregatorReducer.class);
		aggregationU.setOutputKeyClass(Text.class);
		aggregationU.setOutputValueClass(UVMField.class);
		aggregationU.setPartitionerClass(VAggregationPartitioner.class);

		FileInputFormat.addInputPath(aggregationU, inDirU);

		FileOutputFormat.setOutputPath(aggregationU, curatedU);
		aggregationU.waitForCompletion(true);

		//conf.setInt("mapred.reduce.tasks", Constants.reducersNo);

		/**
		 * Picking initial centroids for IMDB
		 * This selection takes place centrally
		 */

		FileStatus[] status = fs.listStatus(inDirIMDB);
		int[][] imdbMoviesFeatures = new int[Constants.imdbMovies][Constants.imdbFeatures];
		//Gathering all info regarding centroids from a folder read by all mappers
		for (int i=0;i<status.length;i++){
			BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(status[i].getPath())));
			String line;
			line=br.readLine();

			while (line != null){
				//System.out.println("IMDB Movies: "+line);
				StringTokenizer itr = new StringTokenizer(line," \t,");
				//1st item in line will be the cluster number
				int movieNo = Integer.parseInt(itr.nextToken());
				while(itr.hasMoreTokens())
				{
					int featurePos = Integer.parseInt(itr.nextToken()) - 1;
					imdbMoviesFeatures[movieNo - 1][featurePos] = 1;
				}
				line=br.readLine();
			}
		}
		System.out.println("All movies read, time to compute initial centroids");

		int[][] imdbCentroids = new int[Constants.imdbK][Constants.imdbFeatures];
		//Pick first randomly
		Random randGen = new Random();

		//Don't want to pick a movie without features in this step
		boolean hasFeatures = false;
		int randomMovie = -1;
		while(hasFeatures == false)
		{
			randomMovie = randGen.nextInt(Constants.imdbMovies);
			imdbCentroids[0] = imdbMoviesFeatures[randomMovie];

			for(int i = 0; i < Constants.imdbFeatures; i++)
			{
				if(imdbCentroids[0][i] != 0)
				{
					hasFeatures = true;
					break;
				}
			}
		}

		//Used to avoid storing same point twice
		List<Integer> currentCentroids = new ArrayList<Integer>(Constants.imdbK);
		currentCentroids.add(randomMovie);


		//Pick the rest
		for(int x = 1; x < Constants.imdbK; x++)
		{
			//We want to find the MAXIMAL minimum distance
			double maxMinDistance = - Double.MAX_VALUE;
			int candidate = -1;
			//minimum distance from selected points


			for(int i = 0 ; i < Constants.imdbMovies; i++)
			{
				int innerCandidate = -1;
				double minDistance = Double.MAX_VALUE;
				if(currentCentroids.contains(i))
				{
					continue;
				}
				//Check all already assigned
				for(int j = 0; j < x; j++)
				{
					double currDistance = 0;
					for(int k = 0; k < Constants.imdbFeatures; k++)
					{
						double diff = (imdbMoviesFeatures[i][k] - imdbCentroids[j][k]);
						currDistance += (diff * diff); 
					}
					currDistance = Math.sqrt(currDistance);
					if(minDistance > currDistance)
					{
						minDistance = currDistance;
						innerCandidate = i;
					}
				}
				if(innerCandidate == -1)
				{
					System.out.println("Error computing distances");
					System.exit(-1);
				}
				if(maxMinDistance < minDistance)
				{
					maxMinDistance = minDistance;
					candidate = innerCandidate;
				}
			}
			if(candidate == -1)
			{
				System.out.println("Error computing initial clusters");
				System.exit(-1);
			}
			imdbCentroids[x] = imdbMoviesFeatures[candidate];
			currentCentroids.add(candidate);
			candidate = -1;
		}
		System.out.println("Centroids picked");
		String[] allEntries = new String[Constants.imdbK];

		//Need to output to HDFS now

		BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(fs.create(imdbClustersInjection, true)));
		for(int i = 0 ; i < Constants.imdbK; i++)
		{
			StringBuffer buf = new StringBuffer();
			buf.append(i);
			buf.append(",");
			for(int j = 0; j < Constants.imdbFeatures; j++)
			{
				if(imdbCentroids[i][j] != 0)
				{
					buf.append(j+1);
					buf.append(",");
					buf.append(1);//Needed due to the output format of my IMDB clusters
					buf.append(",");
				}
			}
			allEntries[i] = buf.substring(0,buf.length() - 1);
			bufferedWriter.write(allEntries[i]);
			bufferedWriter.write("\n");
		}
		bufferedWriter.close();


		/**
		 * Picking initial centroids for Netflix too
		 */
		status = fs.listStatus(curatedV);
		float[][] netflixMoviesFeatures = new float[Constants.imdbMovies][Constants.dimensions];
		//Gathering all info regarding centroids from a folder read by all mappers
		for (int i=0;i<status.length;i++){
			BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(status[i].getPath())));
			String line;
			line=br.readLine();

			while (line != null){

				StringTokenizer itr = new StringTokenizer(line," \t,");
				//1st item in line will be the cluster number
				itr.nextToken();//Discard V
				int movieNo = Integer.parseInt(itr.nextToken());
				for(int j = 0 ; j < Constants.dimensions; j++)
				{
					netflixMoviesFeatures[movieNo - 1][j] = Float.parseFloat(itr.nextToken());
				}
				line=br.readLine();
			}
		}

		float[][] netflixCentroids = new float[Constants.netflixK][Constants.dimensions];

		//Pick first randomly
		int randomNetflixMovie = randGen.nextInt(Constants.imdbMovies);
		netflixCentroids[0] = netflixMoviesFeatures[randomMovie];

		//Used to avoid storing same point twice
		List<Integer> currentNetflixCentroids = new ArrayList<Integer>(Constants.netflixK);
		currentNetflixCentroids.add(randomNetflixMovie);

		//Pick the rest
		for(int x = 1; x < Constants.netflixK; x++)
		{
			//We want to find the MAXIMAL minimum distance
			double maxMinDistance = - Double.MAX_VALUE;
			int candidate = -1;
			//minimum distance from selected points


			for(int i = 0 ; i < Constants.imdbMovies; i++)
			{
				int innerCandidate = -1;
				double minDistance = Double.MAX_VALUE;
				if(currentNetflixCentroids.contains(i))
				{
					continue;
				}
				//Check all already assigned
				for(int j = 0; j < x; j++)
				{
					double currDistance = 0;
					for(int k = 0; k < Constants.dimensions; k++)
					{
						double diff = (netflixMoviesFeatures[i][k] - netflixCentroids[j][k]);
						currDistance += (diff * diff); 
					}
					currDistance = Math.sqrt(currDistance);
					if(minDistance > currDistance)
					{
						minDistance = currDistance;
						innerCandidate = i;
					}
				}
				if(innerCandidate == -1)
				{
					System.out.println("Error computing distances");
					System.exit(-1);
				}
				if(maxMinDistance < minDistance)
				{
					maxMinDistance = minDistance;
					candidate = innerCandidate;
				}
			}
			if(candidate == -1)
			{
				System.out.println("Error computing initial clusters");
				System.exit(-1);
			}
			netflixCentroids[x] = netflixMoviesFeatures[candidate];
			currentNetflixCentroids.add(candidate);
			candidate = -1;
		}
		System.out.println("Netflix Centroids picked");
		String[] allNetflixEntries = new String[Constants.netflixK];

		//Need to output to HDFS now
		bufferedWriter = new BufferedWriter(new OutputStreamWriter(fs.create(netflixClustersInjection, true)));
		for(int i = 0 ; i < Constants.netflixK; i++)
		{
			StringBuffer buf = new StringBuffer();
			buf.append(i);
			buf.append(",");
			for(int j = 0; j < Constants.dimensions; j++)
			{
				buf.append(netflixCentroids[i][j]);
				buf.append(",");

			}
			allNetflixEntries[i] = buf.substring(0,buf.length() - 1);
			//System.out.println("Isolated netflix entry: "+allNetflixEntries[i]);
			bufferedWriter.write(allNetflixEntries[i]);
			bufferedWriter.write("\n");
			//out.writeUTF(allNetflixEntries[i]);
		}
		bufferedWriter.close();

		/**
		 * Random points picking - naive approach, no longer used
		 */
		//		System.out.println("Picking random points - IMDB");
		//		Job imdbCentroids = new Job(conf, "picking centroids - imdb");
		//
		//		imdbCentroids.setJarByClass(MainKMeans.class);
		//		imdbCentroids.setMapperClass(ch.epfl.advdb.mappers.IMDBRandomPointMapper.class);
		//		imdbCentroids.setReducerClass(ch.epfl.advdb.reducers.IMDBRandomPointReducer.class);
		//		imdbCentroids.setOutputKeyClass(Text.class);
		//		imdbCentroids.setOutputValueClass(Text.class);
		//
		//		FileInputFormat.addInputPath(imdbCentroids, inDirIMDB);
		//		FileOutputFormat.setOutputPath(imdbCentroids, imdbCentroidsDir);
		//
		//		imdbCentroids.waitForCompletion(true);
		//
		//		System.out.println("Picking random points - Netflix");
		//		Job netflixCentroids = new Job(conf, "picking centroids - netflix");
		//
		//		netflixCentroids.setJarByClass(MainKMeans.class);
		//		netflixCentroids.setMapperClass(ch.epfl.advdb.mappers.NetflixRandomPointMapper.class);
		//		netflixCentroids.setReducerClass(ch.epfl.advdb.reducers.NetflixRandomPointReducer.class);
		//		netflixCentroids.setOutputKeyClass(Text.class);
		//		netflixCentroids.setOutputValueClass(Text.class);
		//
		//		FileInputFormat.addInputPath(netflixCentroids, curatedV);
		//		FileOutputFormat.setOutputPath(netflixCentroids, netflixCentroidsDir);
		//
		//		netflixCentroids.waitForCompletion(true);


		System.out.println("IMDB iterations");

		int iterIMDB = 0;
		conf.set("imdbCentroidsPath", imdbCentroidsDir.toString());
		for( ; iterIMDB < Constants.iterations; iterIMDB++)
		{
			//KMeans over IMDB data
			Job imdbMeans = new Job(conf, "kmeans - imdb");

			imdbMeans.setJarByClass(Main.class);
			imdbMeans.setMapperClass(ch.epfl.advdb.mappers.IMDBMapper.class);
			imdbMeans.setReducerClass(ch.epfl.advdb.reducers.IMDBReducer.class);
			imdbMeans.setOutputKeyClass(IntWritable.class);
			imdbMeans.setOutputValueClass(IMDBFeatureVector.class);

			FileInputFormat.addInputPath(imdbMeans, inDirIMDB);
			Path centroidsOut = new Path(args[2]+"/../tmp/imdbCentroids_"+(iterIMDB+1));
			FileOutputFormat.setOutputPath(imdbMeans, centroidsOut);

			imdbMeans.waitForCompletion(true);

			//long clustered = imdbMeans.getCounters().findCounter(KMeansCounter.imdbStop).getValue();
			//System.out.println(clustered);

			conf.set("imdbCentroidsPath", centroidsOut.toString());

			//Average radius - used to calibrate
			//long allRadius = imdbMeans.getCounters().findCounter(Constants.KMeansCounter.imdbRadius).getValue();
			//System.out.println("Average radius for IMDB clusters is "+(allRadius / (double)(Constants.imdbK * 10E8)));

		}


		System.out.println("Netflix iterations");

		//Loop after this point
		int iterNetflix = 0;
		conf.set("netflixCentroidsPath", netflixCentroidsDir.toString());
		for( ; iterNetflix < Constants.iterations; iterNetflix++)
		{
			Job netflixMeans = new Job(conf, "kmeans - netflix");

			netflixMeans.setJarByClass(Main.class);
			netflixMeans.setMapperClass(ch.epfl.advdb.mappers.NetflixMapper.class);
			netflixMeans.setReducerClass(ch.epfl.advdb.reducers.NetflixReducer.class);
			netflixMeans.setOutputKeyClass(IntWritable.class);
			netflixMeans.setOutputValueClass(NetflixFeatureVector.class);

			FileInputFormat.addInputPath(netflixMeans, curatedV);
			Path centroidsOut = new Path(args[2]+"/../tmp/netflixCentroids_"+(iterNetflix+1));
			FileOutputFormat.setOutputPath(netflixMeans, centroidsOut);

			netflixMeans.waitForCompletion(true);
			conf.set("netflixCentroidsPath", centroidsOut.toString());

			//			long allRadius = netflixMeans.getCounters().findCounter(Constants.KMeansCounter.netflixRadius).getValue();
			//			System.out.println("Average radius for NETFLIX clusters is "+(allRadius / (double)(Constants.netflixK * 10E8)));
		}


		/**
		 * Matching between IMDB & Netflix
		 */
		System.out.println("MATCHING");

		//Creating Matches
		conf.set("imdbCentroidsPath", outDir+"/../tmp/imdbCentroids_"+Constants.iterations);
		conf.set("netflixCentroidsPath", outDir+"/../tmp/netflixCentroids_"+Constants.iterations);
		Job matching =  new Job(conf, "matching clusters");
		matching.setJarByClass(Main.class);
		matching.setMapperClass(ch.epfl.advdb.mappers.ClusterMatchingMapper.class);
		matching.setReducerClass(ch.epfl.advdb.reducers.ClusterMatchingReducer.class);
		matching.setOutputKeyClass(ClusterPair.class);
		matching.setOutputValueClass(IntWritable.class);
		matching.setPartitionerClass(ClusterPairPartitioner.class);
		matching.setSortComparatorClass(ClusterPairComparator.class);
		matching.setGroupingComparatorClass(ClusterPairGroupComparator.class);

		//Input: Entire IMDB and Netflix
		FileInputFormat.addInputPaths(matching, inDirIMDB.toString()+","+curatedV.toString());
		FileOutputFormat.setOutputPath(matching, matchingOut);
		matching.waitForCompletion(true);

		/**
		 * Feed system with test movies
		 */
		//Mapper: Matching input test IMDB movies with relevant Netflix clusters
		//Reducer: Multiplying
		System.out.println("TESTING");

		conf.set("imdbCentroidsPath", outDir+"/../tmp/imdbCentroids_"+Constants.iterations);
		conf.set("netflixCentroidsPath", outDir+"/../tmp/netflixCentroids_"+Constants.iterations);
		conf.set("matchingsPath", matchingOut.toString());
		conf.setInt("mapred.reduce.tasks", Constants.reducersNo);
		Job testMoviesMatch =  new Job(conf, "feeding test movies");
		testMoviesMatch.setJarByClass(Main.class);
		testMoviesMatch.setMapperClass(ch.epfl.advdb.mappers.TestMoviesMapper.class);
		testMoviesMatch.setReducerClass(ch.epfl.advdb.reducers.TestMoviesReducer.class);
		testMoviesMatch.setOutputKeyClass(TestsKey.class);
		testMoviesMatch.setOutputValueClass(NetflixFeatureVector.class);
		testMoviesMatch.setSortComparatorClass(TestsKeyComparator.class);
		testMoviesMatch.setGroupingComparatorClass(TestsKeyGroupComparator.class);
		testMoviesMatch.setPartitionerClass(TestsKeyPartitioner.class);
		FileInputFormat.addInputPaths(testMoviesMatch, testMoviesDir.toString()+","+curatedU.toString());

		FileOutputFormat.setOutputPath(testMoviesMatch,userSuggestions);

		testMoviesMatch.waitForCompletion(true);

		/**
		 * FScore!
		 */

		//				System.out.println("FSCORE");
		//		
		//				Job aggregationM = new Job(conf, "aggregating values of M");
		//		
		//				aggregationM.setJarByClass(Main.class);
		//				aggregationM.setMapperClass(ch.epfl.advdb.mappers.MAggregatorMapper.class);
		//				aggregationM.setReducerClass(ch.epfl.advdb.reducers.MAggregatorReducer.class);
		//				aggregationM.setOutputKeyClass(Text.class);
		//				aggregationM.setOutputValueClass(UVMField.class);
		//		
		//				FileInputFormat.addInputPath(aggregationM, inDirM);
		//		
		//				FileOutputFormat.setOutputPath(aggregationM, curatedM);
		//				aggregationM.waitForCompletion(true);
		//		
		//				//		if (fs.exists(outDir)) {
		//				//			fs.delete(outDir, true);
		//				//		}
		//				//		fs.mkdirs(outDir);
		//				//		
		//				//		if (fs.exists(scores)) {
		//				//			fs.delete(scores, true);
		//				//		}
		//				//		File[] filesCluster = new File("/home/manolee/hadoopStuff/data/input/remote-matches/deleteme").listFiles();
		//				//		for(File file : filesCluster)
		//				//		{
		//				//			fs.copyFromLocalFile(false, true, new Path(file.getAbsolutePath()),outDir);
		//				//		}
		//		
		//				Job fscore = new Job(conf, "fscore computation");
		//		
		//				fscore.setJarByClass(Main.class);
		//				fscore.setMapperClass(ch.epfl.advdb.mappers.FScoreMapper.class);
		//				fscore.setReducerClass(ch.epfl.advdb.reducers.FScoreReducer.class);
		//				fscore.setOutputKeyClass(Movie.class);
		//				fscore.setOutputValueClass(Text.class);
		//				fscore.setGroupingComparatorClass(MoviesGroupComparator.class);
		//				fscore.setPartitionerClass(MoviesPartitioner.class);
		//				FileInputFormat.addInputPaths(fscore, curatedM.toString()+","+userSuggestions);
		//		
		//				FileOutputFormat.setOutputPath(fscore, scores);
		//				fscore.waitForCompletion(true);
		//		
		//				long fscoreAll = fscore.getCounters().findCounter(Constants.KMeansCounter.fscoreAll).getValue();
		//				long moviesNo = fscore.getCounters().findCounter(Constants.KMeansCounter.nonZero).getValue();
		//				System.out.println("Average fscore for all test movies is NETFLIX clusters is "+(fscoreAll / (double)(Constants.testMoviesNo * 10E8)));
		//				System.out.println("The total was "+(fscoreAll / (double) 10E8)+", "+moviesNo+" out of "+Constants.testMoviesNo+" close to a match");
	}
}

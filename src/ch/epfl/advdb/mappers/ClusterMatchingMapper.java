package ch.epfl.advdb.mappers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

import ch.epfl.advdb.keys.ClusterPair;
import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.util.FloatArrayWritable;
import ch.epfl.advdb.values.Feature;
import ch.epfl.advdb.values.IMDBCentroid;
import ch.epfl.advdb.values.IMDBFeatureVector;
import ch.epfl.advdb.values.MatrixField;
import ch.epfl.advdb.values.NetflixCentroid;
import ch.epfl.advdb.values.NetflixFeatureVector;

public class ClusterMatchingMapper extends Mapper<Object, Text, ClusterPair, IntWritable> {

	//Storing the clusters I'll be reading here
	NetflixCentroid[] centroids = new NetflixCentroid[Constants.netflixK];
	List<IMDBCentroid> clusterFeatures = new ArrayList<IMDBCentroid>(Constants.imdbK);
		
	//Need both types of centroids to be taken into account
	protected void setup(Context context) throws IOException, InterruptedException
	{      
		setupIMDB(context);
		setupNetflix(context);
	} 
	
	protected void setupIMDB(Context context) throws IOException, InterruptedException
	{
		/**
		 * IMDB Clusters
		 */
		String centroidsPath = context.getConfiguration().get("imdbCentroidsPath");

		Path path = new Path(centroidsPath);
		FileSystem fs = path.getFileSystem(context.getConfiguration()); // context of mapper or reducer
		FileStatus[] status = fs.listStatus(path);

		//Gathering all info regarding centroids from a folder read by all mappers
		for (int i=0;i<status.length;i++){
			BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(status[i].getPath())));
			String line;
			line=br.readLine();

			while (line != null){
				//  System.out.println("Map Cache: "+line);


				StringTokenizer itr = new StringTokenizer(line," \t,");
				//1st item in line will be the cluster number
				int centroidNo = Integer.parseInt(itr.nextToken());
				ArrayList<Feature> currentFeats = new ArrayList<Feature>();
				while(itr.hasMoreTokens())
				{
					int featurePos = Integer.parseInt(itr.nextToken()) - 1;
					float featureVal = Float.parseFloat(itr.nextToken());
					currentFeats.add(new Feature(featurePos, featureVal));
				}
				clusterFeatures.add(new IMDBCentroid(centroidNo,currentFeats));
				line=br.readLine();
			}
		}
	}

	
	protected void setupNetflix(Context context) throws IOException, InterruptedException
	{
		/**
		 * NETFLIX Clusters
		 */
		for(int i = 0 ; i < Constants.netflixK; i++)
		{
			centroids[i] = new NetflixCentroid();	
		}
		
		String centroidsPath = context.getConfiguration().get("netflixCentroidsPath");

		Path path = new Path(centroidsPath);
		FileSystem fs = path.getFileSystem(context.getConfiguration()); // context of mapper or reducer
		FileStatus[] status = fs.listStatus(path);

		
		//Gathering all info regarding centroids from a folder read by all mappers
		for (int i=0;i<status.length;i++){
			BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(status[i].getPath())));
			String line;
			line=br.readLine();

			
			while (line != null)
			{
				//System.out.println("Map Cache: "+line);

				StringTokenizer itr = new StringTokenizer(line," \t,");
				//1st item in line will be the cluster number
				//No assumption on sequence of centroids in files
				int centroidNo = Integer.parseInt(itr.nextToken());
				for(int j = 0 ; j < Constants.dimensions; j++)
				{
					centroids[centroidNo].featureVector[j] = Float.parseFloat(itr.nextToken());
				}
				
				line=br.readLine();
			}
		}
	}

	//Either IMDB stuff or netflix stuff need to be submitted to all reducers
	public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

		StringTokenizer itr = new StringTokenizer(value.toString()," \t,");

		int clusterID = 0; //The cluster our current point ends up being assigned to
		
		//If it is equal to 'V', I am dealing with netflix entries
		String indicator = itr.nextToken();
		int mid = -1;
		if(indicator.equals("V")) //Stuff from Netflix
		{
			double maxSimilarity = Double.MIN_VALUE ;

			float numerators[]  = new float[Constants.netflixK];
			float denumeratorsItem = 0;
			float denumeratorsCentroid[] = new float[Constants.netflixK];
			float[] features = new float[Constants.dimensions];	
			int candidate = -1; //Which is the centroid I will end up assigning the item to

			//MovieID
			mid = Integer.parseInt(itr.nextToken());

			
			//No reason to count tokens - I know how many features we have
			for(int i = 0; i < Constants.dimensions; i++)
			{	int count = 0;//centroids count
				float featureVal = Float.parseFloat(itr.nextToken());

				denumeratorsItem += (featureVal * featureVal);

				for(NetflixCentroid centroid : centroids)
				{
					float[] centroidFeatures = centroid.getFeatureVector();
					numerators[count] += featureVal * centroidFeatures[i];
					denumeratorsCentroid[count] += (centroidFeatures[i] * centroidFeatures[i]);
					count++;
				}
				features[i] = featureVal;
			}

			//Have gathered all elems I need - 
			for(int i = 0; i < Constants.netflixK; i++)
			{
				double similarity = numerators[i] / (Math.sqrt(denumeratorsCentroid[i])  * Math.sqrt(denumeratorsItem));
				//System.out.println("Similarity between cluster "+i+" and elem "+mid+": "+similarity);
				if(similarity >= maxSimilarity)
				{
					candidate = i;
					maxSimilarity = similarity;
				}
			}

			if(candidate != -1)
			{
				//System.out.println("For netflix movie "+mid+", candidate "+candidate+" chosen");

				//Send to all any reducer responsible for a cluster for IMDB
				for(int i = 0; i < Constants.imdbK; i++)
				{
					context.write(new ClusterPair(i,candidate), new IntWritable(mid));
				}
				
			}
			else
			{
				System.out.println("Error in matching mapper for netflix - no centroid chosen");
				System.exit(-1);
			}
			
		}
		else //Stuff from IMDB
		{
			mid = Integer.parseInt(indicator);
			
			List<Integer> features = new ArrayList<Integer>(10);
			int candidate = -1; //Which is the centroid I will end up assigning the item to

			double maxSimilarity = Double.MIN_VALUE ;

			float numerators[]  = new float[Constants.imdbK];
			float denumeratorsItem = 0;
			float denumeratorsCentroid[] = new float[Constants.imdbK];

			int count = 0;
			while(itr.hasMoreTokens())
			{
				int featureNo = Integer.parseInt(itr.nextToken()) - 1;

				denumeratorsItem += 1;

				for(IMDBCentroid centroid: clusterFeatures)
				{
					List<Feature> centroidFeatures = centroid.getFeatureVector();
					for(Feature centroidFeat : centroidFeatures)
					{
						float val = centroidFeat.getValue();
						if(count == 0)
						{   //Don't count it multiple times
							denumeratorsCentroid[centroid.getCentroidNo()] += ( val * val );
						}
						if(centroidFeat.getPosition() == featureNo)
						{
							numerators[centroid.getCentroidNo()] += /*1 **/ val;
						}
					}
				}

				features.add(featureNo);
				count++;
			}
			if(count == 0) return;

			//Have gathered all elems I need - 
			for(int i = 0; i < Constants.imdbK; i++)
			{
				double similarity = numerators[i] / (Math.sqrt(denumeratorsCentroid[i])  * Math.sqrt(denumeratorsItem));
				//System.out.println("Similarity between cluster "+i+" and elem "+mid+": "+similarity);
				if(similarity >= maxSimilarity)
				{
					candidate = i;
					maxSimilarity = similarity;
				}
			}

			if(candidate != -1) //Should never happen
			{
				//System.out.println("For imdb movie "+mid+", Candidate "+candidate+" chosen");
				
				context.write(new ClusterPair(candidate,-1), new IntWritable(mid));
			}
			else
			{
				System.out.println("Error in matching mapper for imdb - no centroid chosen");
				System.exit(-1);
			}
			//In the case of no centroid chosen, it is probable that no feature concerning movie was available (e.g. movie 5)

			return;
			
		}

	}


}
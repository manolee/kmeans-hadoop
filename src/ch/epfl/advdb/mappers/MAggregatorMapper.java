package ch.epfl.advdb.mappers;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.values.UVMField;

//Used to aggregate utility matrix when we needed to compute Fscore
public class MAggregatorMapper extends Mapper<Object, Text, Text, UVMField> {

	public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

		StringTokenizer itr = new StringTokenizer(value.toString()," \t,");

		StringBuffer buf = new StringBuffer();

		String row = itr.nextToken();
		String col = itr.nextToken();
		String grade = itr.nextToken();

		if(Float.parseFloat(grade) >= 0)
		{
			context.write(new Text(col), new UVMField(row, col, grade));
		}

	}
}
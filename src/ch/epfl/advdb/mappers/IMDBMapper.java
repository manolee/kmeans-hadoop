package ch.epfl.advdb.mappers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.util.FloatArrayWritable;
import ch.epfl.advdb.values.IMDBCentroid;
import ch.epfl.advdb.values.Feature;
import ch.epfl.advdb.values.IMDBFeatureVector;
import ch.epfl.advdb.values.MatrixField;

public class IMDBMapper extends Mapper<Object, Text, IntWritable, IMDBFeatureVector> {

	//Storing the clusters I'll be reading here
	List<IMDBCentroid> clusterFeatures = new ArrayList<IMDBCentroid>(Constants.imdbK); 

	//Read centroids of IMDB
	protected void setup(Context context) throws IOException, InterruptedException
	{      

		String centroidsPath = context.getConfiguration().get("imdbCentroidsPath");

		Path path = new Path(centroidsPath);
		FileSystem fs = path.getFileSystem(context.getConfiguration()); // context of mapper or reducer
		FileStatus[] status = fs.listStatus(path);

		//Gathering all info regarding centroids from a folder read by all mappers
		for (int i=0;i<status.length;i++){
			BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(status[i].getPath())));
			String line;
			line=br.readLine();

			while (line != null){
				//System.out.println("IMDB Map Cache: "+line);

				StringTokenizer itr = new StringTokenizer(line," \t,");
				//1st item in line will be the cluster number
				int centroidNo = Integer.parseInt(itr.nextToken());
				ArrayList<Feature> currentFeats = new ArrayList<Feature>();
				while(itr.hasMoreTokens())
				{
					int featurePos = Integer.parseInt(itr.nextToken()) - 1;
					float featureVal = Float.parseFloat(itr.nextToken());
					currentFeats.add(new Feature(featurePos, featureVal));
				}
				clusterFeatures.add(new IMDBCentroid(centroidNo,currentFeats));
				line=br.readLine();
			}
		}

	} 

	public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

		List<Integer> features = new ArrayList<Integer>(10);//10 is an arbitrary number, not connected with my centroids
		int candidate = -1; //Which is the centroid I will end up assigning the item to

		double maxSimilarity = - Double.MAX_VALUE ;

		float numerators[]  = new float[Constants.imdbK];
		float denumeratorsCentroid[] = new float[Constants.imdbK];
		float denumeratorsItem = 0;
		

		StringTokenizer itr = new StringTokenizer(value.toString()," \t,");

		int clusterID = 0; //The cluster our current point ends up being assigned to

		//MovieID
		int mid = Integer.parseInt(itr.nextToken());

		int count = 0;
		while(itr.hasMoreTokens())
		{
			//In the dataset values begin from 1 instead of 0
			int featureNo = Integer.parseInt(itr.nextToken()) - 1; //All values are 1 - what changes is their position

			denumeratorsItem += 1;

			for(IMDBCentroid centroid: clusterFeatures)
			{
				List<Feature> centroidFeatures = centroid.getFeatureVector();
				for(Feature centroidFeat : centroidFeatures)
				{
					float val = centroidFeat.getValue();
					if(count == 0)
					{   //Don't count it multiple times
						denumeratorsCentroid[centroid.getCentroidNo()] += ( val * val );
					}
					if(centroidFeat.getPosition() == featureNo)
					{
						numerators[centroid.getCentroidNo()] += /*1 **/ val;
					}
				}
			}

			//Sending the "normalized" featureNo (starting from 0)
			features.add(featureNo);
			count++;
		}
		if(count == 0) return;

		//Have gathered all elems I need - 
		for(int i = 0; i < Constants.imdbK; i++)
		{
			double similarity = numerators[i] / (Math.sqrt(denumeratorsCentroid[i])  * Math.sqrt(denumeratorsItem));
			//System.out.println("Similarity between cluster "+i+" and elem "+mid+": "+similarity);
			if(similarity >= maxSimilarity)
			{
				candidate = i;
				maxSimilarity = similarity;
			}
		}

		if(candidate != -1) //Should never happen
		{
			//System.out.println("For movie "+mid+", Cluster/Candidate "+candidate+" chosen - similarity: "+maxSimilarity);
			context.write(new IntWritable(candidate), new IMDBFeatureVector(mid,count,features));
		}
		else
		{
			System.out.println("What about imdb movie  "+mid+"??");
			System.out.println("Error in KMeans mapper - imdb - no centroid chosen");
			System.exit(-1);
		}
		//In the case of no centroid chosen, it is probable that no feature concerning movie was available (e.g. movie 5)

		return;



	}




}
package ch.epfl.advdb.mappers;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.values.IMDBFeatureVector;
import ch.epfl.advdb.values.MatrixField;

/**
 * 
 * @author manolee
 * Not used at the moment. Used as a first approach to calculate initial centroids
 */
public class IMDBRandomPointMapper extends Mapper<Object, Text, Text, Text> {


	
	public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

		StringTokenizer itr = new StringTokenizer(value.toString()," \t,");

		
		int mid = Integer.parseInt(itr.nextToken());



		context.write(new Text((mid % Constants.imdbK)+""), value);


	}
}
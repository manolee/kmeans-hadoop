package ch.epfl.advdb.mappers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.util.FloatArrayWritable;
import ch.epfl.advdb.values.Feature;
import ch.epfl.advdb.values.IMDBFeatureVector;
import ch.epfl.advdb.values.MatrixField;
import ch.epfl.advdb.values.NetflixCentroid;
import ch.epfl.advdb.values.NetflixFeatureVector;

public class NetflixMapper extends Mapper<Object, Text, IntWritable, NetflixFeatureVector> {

	//Storing the clusters I'll be reading here
	NetflixCentroid[] centroids = new NetflixCentroid[Constants.netflixK];
	
	protected void setup(Context context) throws IOException, InterruptedException
	{      
		
		for(int i = 0 ; i < Constants.netflixK; i++)
		{
			centroids[i] = new NetflixCentroid();	
		}
		
		String centroidsPath = context.getConfiguration().get("netflixCentroidsPath");

		Path path = new Path(centroidsPath);
		FileSystem fs = path.getFileSystem(context.getConfiguration()); // context of mapper or reducer
		FileStatus[] status = fs.listStatus(path);

		
		//Gathering all info regarding centroids from a folder read by all mappers
		for (int i=0;i<status.length;i++){
			BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(status[i].getPath())));
			String line;
			line=br.readLine();

			
			while (line != null)
			{
				//System.out.println("Netflix Map Cache: "+line);

				StringTokenizer itr = new StringTokenizer(line," \t,");
				//1st item in line will be the cluster number
				//No assumption on sequence of centroids in files
				int centroidNo = Integer.parseInt(itr.nextToken());
				for(int j = 0 ; j < Constants.dimensions; j++)
				{
					centroids[centroidNo].featureVector[j] = Float.parseFloat(itr.nextToken());
				}
				
				line=br.readLine();
			}
		}

	} 

	public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

		float[] features = new float[Constants.dimensions];	
		int candidate = -1; //Which is the centroid I will end up assigning the item to

		double maxSimilarity = - Double.MAX_VALUE ;

		float numerators[]  = new float[Constants.netflixK];
		float denumeratorsItem = 0;
		float denumeratorsCentroid[] = new float[Constants.netflixK];

		StringTokenizer itr = new StringTokenizer(value.toString()," \t,");

		int clusterID = 0; //The cluster our current point ends up being assigned to

		//MovieID
		itr.nextToken(); //Ignore - it will just be letter 'V'
		int mid = Integer.parseInt(itr.nextToken());

		
		//No reason to count tokens - I know how many features we have
		for(int i = 0; i < Constants.dimensions; i++)
		{	int count = 0;//centroids count
			float featureVal = Float.parseFloat(itr.nextToken());

			denumeratorsItem += (featureVal * featureVal);

			for(NetflixCentroid centroid : centroids)
			{
				float[] centroidFeatures = centroid.getFeatureVector();
				numerators[count] += featureVal * centroidFeatures[i];
				denumeratorsCentroid[count] += (centroidFeatures[i] * centroidFeatures[i]);
				count++;
			}
			features[i] = featureVal;
		}

		//Have gathered all elems I need - 
		double similarity = 0;
		for(int i = 0; i < Constants.netflixK; i++)
		{
			similarity = numerators[i] / (Math.sqrt(denumeratorsCentroid[i])  * Math.sqrt(denumeratorsItem));
			//System.out.println("Similarity between cluster "+i+" and elem "+mid+": "+similarity);
			if(similarity >= maxSimilarity)
			{
				candidate = i;
				maxSimilarity = similarity;
			}
		}

		if(candidate != -1) //Should never happen
		{
			//System.out.println("For movie "+mid+", Cluster/Candidate "+candidate+" chosen - similarity: "+maxSimilarity);
			context.write(new IntWritable(candidate), new NetflixFeatureVector(mid,features));
		}
		else
		{
			System.out.println("What about netflix movie  "+mid+"??");
			System.out.println("Error in KMeans mapper - netflix - no centroid chosen");
			System.exit(-1);
		}

		return;

	}


}
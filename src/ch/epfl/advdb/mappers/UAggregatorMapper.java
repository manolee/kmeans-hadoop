package ch.epfl.advdb.mappers;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.values.UVMField;

//Used to subsequently store matrix V in rows instead of per-element, each row storing a single column of the matrix
//Also used to aggregate utility matrix when we needed to compute Fscore
public class UAggregatorMapper extends Mapper<Object, Text, Text, UVMField> {

	public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

		StringTokenizer itr = new StringTokenizer(value.toString()," \t,");

		StringBuffer buf = new StringBuffer();

		//MovieID
		String mid = itr.nextToken();
		if(mid.equals("E"))
		{
			//bogus entry - just return
			return;
		}
		
		String row = itr.nextToken();
		String col = itr.nextToken();
		String grade = itr.nextToken();

		context.write(new Text(row), new UVMField(row, col, grade));

	}
}
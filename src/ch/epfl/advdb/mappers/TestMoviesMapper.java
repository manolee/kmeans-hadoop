package ch.epfl.advdb.mappers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.hadoop.filecache.DistributedCache;
import org.apache.hadoop.fs.FSDataInputStream;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

import ch.epfl.advdb.keys.ClusterPair;
import ch.epfl.advdb.keys.TestsKey;
import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.util.FloatArrayWritable;
import ch.epfl.advdb.values.IMDBCentroid;
import ch.epfl.advdb.values.Feature;
import ch.epfl.advdb.values.IMDBFeatureVector;
import ch.epfl.advdb.values.MatrixField;
import ch.epfl.advdb.values.NetflixFeatureVector;

/**
 * 
 * @author manolee
 * Used when program is fed test movies.
 * Mappers read centroids of IMDB, Netflix and their matches at setup time.
 * Each movie arriving is associated with both datasets, and the pairing is submitted to the appropriate reducer
 */
public class TestMoviesMapper extends Mapper<Object, Text, TestsKey, NetflixFeatureVector> {

	//Storing the clusters I'll be reading here
	List<IMDBCentroid> clusterFeatures = new ArrayList<IMDBCentroid>(Constants.imdbK); 
	ClusterPair[] matches = new ClusterPair[Constants.imdbK];
	float[][] netflixCentroids = new float[Constants.netflixK][Constants.dimensions];

	protected void setup(Context context) throws IOException, InterruptedException
	{      

		String centroidsPath = context.getConfiguration().get("imdbCentroidsPath");

		Path path = new Path(centroidsPath);
		FileSystem fs = path.getFileSystem(context.getConfiguration()); // context of mapper or reducer
		FileStatus[] status = fs.listStatus(path);

		//Gathering all info regarding centroids from a folder read by all mappers
		for (int i=0;i<status.length;i++){
			BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(status[i].getPath())));
			String line;
			line=br.readLine();

			while (line != null){
				//  System.out.println("Map Cache: "+line);


				StringTokenizer itr = new StringTokenizer(line," \t,");
				//1st item in line will be the cluster number
				int centroidNo = Integer.parseInt(itr.nextToken());
				ArrayList<Feature> currentFeats = new ArrayList<Feature>();
				while(itr.hasMoreTokens())
				{
					int featurePos = Integer.parseInt(itr.nextToken()) - 1;
					float featureVal = Float.parseFloat(itr.nextToken());
					currentFeats.add(new Feature(featurePos, featureVal));
				}
				clusterFeatures.add(new IMDBCentroid(centroidNo,currentFeats));
				line=br.readLine();
			}
		}

		//Load netflix centroids
		String netflixCentroidsPath = context.getConfiguration().get("netflixCentroidsPath");

		path = new Path(netflixCentroidsPath);
		status = fs.listStatus(path);

		//Gathering all info regarding centroids from a folder read by all mappers
		for (int i=0;i<status.length;i++) {
			BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(status[i].getPath())));
			String line;
			line=br.readLine();


			while(line != null)
			{
				StringTokenizer itr = new StringTokenizer(line," \t,");
				int centroidNo = Integer.parseInt(itr.nextToken());

				while(itr.hasMoreTokens())
				{
					for(int j = 0 ; j < Constants.dimensions; j++)
					{
						netflixCentroids[centroidNo][j] = Float.parseFloat(itr.nextToken());
					}
				}

				line=br.readLine();
			}
		}

		//Load matching info too
		String matchingsPath = context.getConfiguration().get("matchingsPath");

		path = new Path(matchingsPath);
		status = fs.listStatus(path);

		//Gathering all info regarding centroids from a folder read by all mappers
		int count = 0;
		for (int i=0;i<status.length;i++) {
			BufferedReader br=new BufferedReader(new InputStreamReader(fs.open(status[i].getPath())));
			String line;
			line=br.readLine();

			while(line != null)
			{
				StringTokenizer itr = new StringTokenizer(line," \t,");
				int imdbCentroidNo = Integer.parseInt(itr.nextToken());
				int netflixCentroidNo = Integer.parseInt(itr.nextToken());
				matches[count++] = new ClusterPair(imdbCentroidNo, netflixCentroidNo);

				line=br.readLine();
			}
		}

	} 

	public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

		List<Integer> features = new ArrayList<Integer>(10);//10: Not to be related with number of dimensions - just number deemed appropriate
		int candidate = -1; //Which is the centroid I will end up assigning the item to

		double maxSimilarity = Double.MIN_VALUE ;

		float numerators[]  = new float[Constants.imdbK];
		float denumeratorsItem = 0;
		float denumeratorsCentroid[] = new float[Constants.imdbK];

		StringTokenizer itr = new StringTokenizer(value.toString()," \t,");

		String ident = itr.nextToken();

		if(ident.equals("U"))
		{
			//Matrix U -> Send to all
			int rowID = Integer.parseInt(itr.nextToken());
			float[] UCols = new float[Constants.dimensions];
			for(int i = 0; i< Constants.dimensions; i++)
			{
				UCols[i] = Float.parseFloat(itr.nextToken());
			}

			for(int i = 0; i < Constants.netflixK/*Constants.reducersNo*/; i++)
			{
				context.write(new TestsKey(i,1), new NetflixFeatureVector(rowID,UCols));
			}
		}
		else
		{
			//MovieID
			int mid = Integer.parseInt(ident);

			int count = 0;
			while(itr.hasMoreTokens())
			{
				int featureNo = Integer.parseInt(itr.nextToken()) - 1;

				denumeratorsItem += 1;

				for(IMDBCentroid centroid: clusterFeatures)
				{
					List<Feature> centroidFeatures = centroid.getFeatureVector();
					for(Feature centroidFeat : centroidFeatures)
					{
						float val = centroidFeat.getValue();
						if(count == 0)
						{   //Don't count it multiple times
							denumeratorsCentroid[centroid.getCentroidNo()] += ( val * val );
						}
						if(centroidFeat.getPosition() == featureNo)
						{
							numerators[centroid.getCentroidNo()] += /*1 **/ val;
						}
					}
				}

				features.add(featureNo);
				count++;
			}
			if(count == 0) return;

			//Have gathered all elems I need - 
			for(int i = 0; i < Constants.imdbK; i++)
			{
				double similarity = numerators[i] / (Math.sqrt(denumeratorsCentroid[i])  * Math.sqrt(denumeratorsItem));
				//System.out.println("Similarity between cluster "+i+" and elem "+mid+": "+similarity);
				if(similarity >= maxSimilarity)
				{
					candidate = i;
					maxSimilarity = similarity;
				}
			}

			if(candidate == -1) //Should never happen
			{
				System.out.println("Error in KMeans mapper - no centroid chosen");
				System.exit(-1);

			}

			//System.out.println("For movie "+mid+", Candidate "+candidate+" chosen");

			//Find match
			int matchingCluster = matches[candidate].getClusterNetflix();//getMatchingInfo(candidate,context);

			//Get netflix centroid info
			float[] centroid = netflixCentroids[matchingCluster];//getCentroidInfo(matchingCluster, context);

			//Send all movies corresponding to same matchingCluster to same reducer
			context.write(new TestsKey(matchingCluster , 0), new NetflixFeatureVector(mid,centroid));
		}
	}


}
package ch.epfl.advdb.mappers;

import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Mapper.Context;

import ch.epfl.advdb.keys.Movie;
import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.values.UVMField;

/**
 * 
 * @author manolee
 * Not used in delivered version
 * Receiving both 'curated' M and our results in order to compute FScore
 */

public class FScoreMapper extends Mapper<Object, Text, Movie, Text> {

	public void map(Object key, Text value, Context context) throws IOException, InterruptedException {

		StringTokenizer itr = new StringTokenizer(value.toString()," \t,");

		StringBuffer buf = new StringBuffer();

		String first = itr.nextToken();
		
		//Testing dataset
		String mid;
		char matrixType = 'M';
		if(first.equals("M"))
		{
			mid = itr.nextToken();
		}
		else//our results
		{
			matrixType = 'X';
			mid = first;
		}
		
		while(itr.hasMoreTokens())
		{
			buf.append(itr.nextToken());
			buf.append(',');
		}
		
		String result = buf.substring(0,buf.length() - 1);
		context.write(new Movie(Integer.parseInt(mid),matrixType), new Text(result));
		

	}
}
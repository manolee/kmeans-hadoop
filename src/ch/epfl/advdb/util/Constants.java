package ch.epfl.advdb.util;

import org.apache.hadoop.mapreduce.Counter;


/**
 * Information used throughout the application,
 * such as number of reducers etc.
 * @author manolee
 *
 */
public class Constants {

	//LARGE
	public static int iterations = 10;//Iterations number
	public static int reducersNo = 44;//Number of reducers utilized @ cluster 

	public static int utilMatrixUsers = 17194040;//190152;
	public static int imdbK = 50;//25;//20;
	public static int netflixK = 50;//25;//30;

	public static int imdbFeatures = 1852;

	//V-Specific. Differs between large and small
	//Using it when locating initial centroids
	public static int imdbMovies = 8146;//60; 

	//Just for printout reasons - no functionality difference
	public static int testMoviesNo = 2715;

	//SMALL
	//		public static int iterations = 5;
	//		public static int reducersNo = 44;//88;//Number of reducers utilized @ cluster 
	//	
	//		public static int utilMatrixUsers = 190152;
	//		public static int imdbK = 5;//50;//25;//20;
	//		public static int netflixK = 5;//50;//25;//30;
	//		
	//		public static int imdbFeatures = 1852;
	//		
	//		//V-Specific. Differs between large and small
	//		//Using it when locating initial centroids
	//		public static int imdbMovies = 60;//8146;//60; 
	//		
	//		
	//		//public static int firstMovieNo = 61;
	//		
	//		//Just for optimization reasons - no functionality difference
	//		public static int testMoviesNo = 20;


	//Note: in the case of radius, i will multiply with large number and then set back to initial => Treat value as int
	public static enum KMeansCounter { imdbStop , netflixStop, imdbRadius, netflixRadius , fscoreAll, nonZero};
	public static int dimensions = 10;

}

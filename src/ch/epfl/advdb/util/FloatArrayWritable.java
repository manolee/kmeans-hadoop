package ch.epfl.advdb.util;

import org.apache.hadoop.io.ArrayWritable;
import org.apache.hadoop.io.FloatWritable;
import org.apache.hadoop.io.Writable;

/**
 * 
 * @author manolee
 *
 */

public class FloatArrayWritable extends ArrayWritable {

	public FloatArrayWritable(Writable[] values) {
		super(FloatWritable.class, values);
	}

	public FloatArrayWritable() {
		super(FloatWritable.class);
	}

	public FloatArrayWritable(Class valueClass, Writable[] values) {
		super(FloatWritable.class, values);
	}

	public FloatArrayWritable(Class valueClass) {
		super(FloatWritable.class);
	}

	public FloatArrayWritable(String[] strings) {
		super(strings);
	}

}

//Adapted from gcu.googlecode.com/files/IntArrayWritable.java
/*
 * Copyright © 2008 California Polytechnic State University
 * Licensed under the Creative Commons 
 * Attribution 3.0 License -http://creativecommons.org/licenses/by/3.0/
 */
package ch.epfl.advdb.util;


import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
import org.apache.hadoop.mapreduce.Partitioner;

import ch.epfl.advdb.keys.ClusterPair;
import ch.epfl.advdb.keys.Movie;
import ch.epfl.advdb.keys.TestsKey;
import ch.epfl.advdb.values.NetflixFeatureVector;
import ch.epfl.advdb.values.UVMField;

/**
 * Various comparators used whenever we introduce
 * a more 'complex' key, or we want to achieve some secondary sorting
 * @author manolee
 *
 */
public class KMeansComparators {

	//Probably not of importance here
	//Controlling physical node values will end up at
	public static class VAggregationPartitioner extends Partitioner<Text, UVMField>
	{
		public int getPartition (Text key, UVMField value, int numPartitions) {

			int partKey = Integer.parseInt(key.toString()) % (numPartitions);
			return partKey;
		}
	}


	public static class ClusterPairPartitioner extends Partitioner<ClusterPair, IntWritable>
	{
		public int getPartition (ClusterPair key, IntWritable value, int numPartitions) {

			int partKey = key.getClusterIMDB() % numPartitions;
			return partKey;
		}
	}

	//Just calls original compare method - used so that KeyComparator does not alter our 'buckets'
	public static class ClusterPairGroupComparator extends WritableComparator {
		protected ClusterPairGroupComparator() {
			super(ClusterPair.class, true);
		}
		@Override
		public int compare(WritableComparable w1, WritableComparable w2) {
			ClusterPair ip1 = (ClusterPair) w1;
			ClusterPair ip2 = (ClusterPair) w2;
			return ClusterPair.compare(ip1, ip2);

		}
	}

	//Used to dictate order of input values
	//The entire IMDB info will arrive first.
	//Then, Netflix info will arrive, sorted per clusterNo
	public static class ClusterPairComparator extends WritableComparator {
		protected ClusterPairComparator() {
			super(ClusterPair.class, true);
		}
		@Override
		public int compare(WritableComparable w1, WritableComparable w2) {
			ClusterPair ip1 = (ClusterPair) w1;
			ClusterPair ip2 = (ClusterPair) w2;
			int cmp = ClusterPair.compare(ip1, ip2);
			if(cmp != 0)
			{
				//i.e. not in same bucket / reducer -=> don't mess with them
				return cmp;
			}

			if(ip1.getClusterNetflix() < ip2.getClusterNetflix())
				return -1;
			else if (ip1.getClusterNetflix() > ip2.getClusterNetflix())
				return 1;

			return cmp;

		}
	}



	public static class TestsKeyPartitioner extends Partitioner<TestsKey, NetflixFeatureVector>
	{
		public int getPartition (TestsKey key, NetflixFeatureVector value, int numPartitions) {

			int partKey = /*numPartitions > 1 ? key.getIndex() :*/ key.getIndex() % numPartitions;
			return partKey;
		}
	}
	//Just calls original compare method - used so that KeyComparator does not alter our 'buckets'
	public static class TestsKeyGroupComparator extends WritableComparator {
		protected TestsKeyGroupComparator() {
			super(TestsKey.class, true);
		}
		@Override
		public int compare(WritableComparable w1, WritableComparable w2) {
			TestsKey ip1 = (TestsKey) w1;
			TestsKey ip2 = (TestsKey) w2;
			return TestsKey.compare(ip1, ip2);

		}
	}

	public static class TestsKeyComparator extends WritableComparator {
		protected TestsKeyComparator() {
			super(TestsKey.class, true);
		}
		@Override
		public int compare(WritableComparable w1, WritableComparable w2) {
			TestsKey ip1 = (TestsKey) w1;
			TestsKey ip2 = (TestsKey) w2;
			int cmp = TestsKey.compare(ip1, ip2);
			if(cmp != 0)
			{
				return cmp;
			}

			if(ip1.getType() != ip2.getType())
			{
				return ip1.getType() == 0 ? -1 : 1;
			}
			//				
			//				if(ip1.getType() < ip2.getType())
			//					return -1;
			//				else if (ip1.getType() > ip2.getType())
			//					return 1;

			return cmp;

		}
	}


	public static class MoviesGroupComparator extends WritableComparator {
		protected MoviesGroupComparator() {
			super(Movie.class, true);
		}
		@Override
		public int compare(WritableComparable w1, WritableComparable w2) {
			Movie ip1 = (Movie) w1;
			Movie ip2 = (Movie) w2;
			return Movie.compare(ip1, ip2);

		}
	}


	public static class MoviesPartitioner extends Partitioner<Movie, Text>
	{
		public int getPartition (Movie key, Text value, int numPartitions) {

			int partKey = key.getMid() % (numPartitions);
			return partKey;
		}
	}

}

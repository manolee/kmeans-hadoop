package ch.epfl.advdb.reducers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.values.Feature;
import ch.epfl.advdb.values.IMDBFeatureVector;


/**
 * 
 * @author manolee
 * 
 */
public class IMDBReducer extends Reducer<IntWritable,IMDBFeatureVector,IntWritable,Text> {

	public void reduce(IntWritable key, Iterable<IMDBFeatureVector> values, Context context) throws IOException, InterruptedException {
		List<Integer[]> infoForRadius = new ArrayList<Integer[]>();

		int[] allFeatures = new int[Constants.imdbFeatures];

		int itemsInCluster = 0;

		for(IMDBFeatureVector item : values)
		{
			infoForRadius.add(item.getFeatures().clone());
			for(Integer featNo : item.getFeatures())
			{
				allFeatures[featNo] += 1;
			}
			itemsInCluster++;
		}

		//Time to writeout new cluster centroid
		StringBuffer value = new StringBuffer();
		for(int i = 0; i < Constants.imdbFeatures; i++ )
		{
			if(allFeatures[i]!=0)
			{
				value.append(i + 1);//Position of feature
				value.append(',');
				value.append(allFeatures[i] / (float) itemsInCluster );
				value.append(',');
				//Will need to crop last character of buffer due to trailing comma
			}
		}

		//stopping condition - Not active after all
		//		context.getCounter(Constants.KMeansCounter.imdbStop).increment(1);

		//System.out.println("New centroid: "+value.substring(0,value.length() - 1).toString());
		context.write(key, new Text(value.substring(0,value.length() - 1).toString()));

		//		//Radius computation
		//		double maxRadius = 0;
		//		double currRadius = 0;
		//		
		//		//int[] tmp = new int[]
		//		for(Integer[] point : infoForRadius)
		//		{
		//			currRadius = 0;
		//			for(Integer coord : point)
		//			{
		//				allFeatures[coord] -= 1;
		//			}
		//			
		//			for(int i = 0 ; i < Constants.imdbFeatures; i++)
		//			{
		//				currRadius += (allFeatures[i] * allFeatures[i]);
		//			}
		//			currRadius = Math.sqrt(currRadius);
		//			maxRadius = currRadius > maxRadius ? currRadius : maxRadius;
		//		}
		//		context.getCounter(Constants.KMeansCounter.imdbRadius).increment((long) (maxRadius * 10E8));

	}

}
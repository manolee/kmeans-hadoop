package ch.epfl.advdb.reducers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.values.MatrixField;

public class IMDBRandomPointReducer extends Reducer<Text,Text,Text,Text> {

	public void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {

		//context.write(new Text(newKey), new Text(buf.toString()));
		List<String> allValues = new ArrayList<String>();

		for(Text val : values)
		{
			allValues.add(val.toString());
		}

		Random randGen = new Random();
		String randomCenter = allValues.get(randGen.nextInt(allValues.size()));


		StringTokenizer itr = new StringTokenizer(randomCenter.toString()," \t,");


		int mid = Integer.parseInt(itr.nextToken());
		StringBuffer buf = new StringBuffer();
		//Reason: Don't want to pick a movie with no features
		while(true)
		{
			if(itr.hasMoreTokens())
			{
				while(itr.hasMoreTokens())
				{
					buf.append(itr.nextToken());
					buf.append(',');
					buf.append(1);
					buf.append(',');
				}

				break;
			}
			randomCenter = allValues.get(randGen.nextInt(allValues.size()));
			itr = new StringTokenizer(randomCenter.toString()," \t,");
		}

		context.write(key, new Text(buf.substring(0,buf.length() - 1)));

	}
}
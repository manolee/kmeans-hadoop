package ch.epfl.advdb.reducers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import ch.epfl.advdb.keys.ClusterPair;
import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.values.Feature;
import ch.epfl.advdb.values.IMDBFeatureVector;


/**
 * 
 * @author manolee
 * 
 */
public class ClusterMatchingReducer extends Reducer<ClusterPair,IntWritable,Text,Text> {

	public void reduce(ClusterPair key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
		
		//Used to perform unions and intersections without wasting too much space
		//BitSet moviesIMDB = new BitSet();
		float maxJaccard = -1;
		int candidate = -1;
		//moviesIMDB.set()
		//System.out.println("Matching reducer reached");
		
		Iterator iter = values.iterator();
		
		BitSet moviesIMDB = new BitSet();
		BitSet moviesNetflix = new BitSet(); //will be renewed for every new clusterNo occurring
		int prevVal = 0;
		int netflixKey = -1;
		int netflixStart = 0;
		while(iter.hasNext())
		{
			int val = ((IntWritable) iter.next()).get();
			netflixKey = key.getClusterNetflix();
			//System.out.println(key.toString()+" for movie "+val);
			if(netflixKey == -1)
			{
				//Still setting initial bitset
				moviesIMDB.set(val);
				//prevVal = netflixKey;
				continue;
			}
			
			if(netflixStart == 0)
			{
				netflixStart = 1;
				moviesNetflix.clear();
			}
			
			//Only netflix part from here on
			
			
			
			if(prevVal != netflixKey)
			{
				//int placeholder = -7;
				//Checking candidate before starting work on next one
				//if(prevVal >= 0)
				//{
					BitSet numerator = (BitSet) moviesIMDB.clone();
					BitSet denominator = (BitSet) moviesIMDB.clone();
					numerator.and(moviesNetflix);
					denominator.or(moviesNetflix);
					
					float currJaccard = numerator.cardinality() / (float) denominator.cardinality();
//					System.out.println("IMDB: "+moviesIMDB.toString());
//					
//					System.out.println("Netflix: "+moviesNetflix.toString());
//					System.out.println("------------");
//					System.out.println("Jaccard between imdb's "+key.getClusterIMDB()+" and netflix's "+(netflixKey - 1)+": "+currJaccard);
//					System.out.println("------------");
					if(currJaccard >= maxJaccard)
					{
						maxJaccard = currJaccard;
						candidate = netflixKey - 1; 
					}
					//netflixStart = 0;
					moviesNetflix.clear();
				//}
				
				
			}
			moviesNetflix.set(val);
			
			//moviesNetflix.set(val);
			prevVal = netflixKey;
		}
		
		
		
		//Last span was not tested
		if(prevVal >= 0)
		{
			BitSet numerator = (BitSet) moviesIMDB.clone();
			BitSet denominator = (BitSet) moviesIMDB.clone();
			numerator.and(moviesNetflix);
			denominator.or(moviesNetflix);
			
			float currJaccard = numerator.cardinality() / (float) denominator.cardinality();
//			System.out.println("IMDB: "+moviesIMDB.toString());
//			System.out.println("Netflix: "+moviesNetflix.toString());
//			System.out.println("------------");
//			System.out.println("Jaccard between imdb's "+key.getClusterIMDB()+" and netflix's "+netflixKey+": "+currJaccard);
//			System.out.println("------------");
			if(currJaccard >= maxJaccard)
			{
				maxJaccard = currJaccard;
				candidate = netflixKey; 
			}
			
		}
		
		if(candidate != -1)
		{
			//System.out.println("Paired imdb's cluster "+key.getClusterIMDB()+" with netflix's "+candidate);
			context.write(new Text(key.getClusterIMDB()+""),new Text(candidate+""));
		}
		else
		{
			System.out.println("Error in cluster matcher - no matching found");
			System.exit(-1);
		}
		
//		for(IntWritable val : values)
//		{
//			System.out.println(key.toString()+" for movie "+val);
//		}
		
	}
}
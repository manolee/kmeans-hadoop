package ch.epfl.advdb.reducers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;

import ch.epfl.advdb.keys.TestsKey;
import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.values.Feature;
import ch.epfl.advdb.values.IMDBFeatureVector;
import ch.epfl.advdb.values.NetflixFeatureVector;


/**
 * 
 * @author manolee
 * Used to assign test IMDB movies to appropriate cluster center from Netflix
 * Every reducer ends up with all movies related to a specific centroid
 * Then, performing the multiplication with the user vectors can only be performed once
 */
public class TestMoviesReducer extends Reducer<TestsKey,NetflixFeatureVector,Text,Text> {

	public void reduce(TestsKey key, Iterable<NetflixFeatureVector> values, Context context) throws IOException, InterruptedException {

		List<Integer> movies = new ArrayList<Integer>();
		List<Integer> users = new ArrayList<Integer>();

		boolean testMovies = false;
		Iterator iter = values.iterator();

		//We first store multiple centroids of interest. Then will gather info for each one of them
		NetflixFeatureVector netflixCentroid = null;
		NetflixFeatureVector movie = (NetflixFeatureVector) iter.next();

		boolean init = false;
		//Movies arrive first
		while(key.getType() == 0) 
		{
			if(init == false)
			{
				init = true;
				netflixCentroid = new NetflixFeatureVector(movie.getMovieID(),movie.getFeatures().clone());
			}
			movies.add(movie.getMovieID());
			movie = (NetflixFeatureVector) iter.next();
			testMovies = true;
		}

		//If no movies received: Exit reducer
		if(!testMovies)
		{
			return;
		}

		NetflixFeatureVector user = movie;
		//have already consumed a movie, so I apply this code for the first time
		float rating = 0;
		float[] centroidVector = netflixCentroid.getFeatures();
		float[] userVector = movie.getFeatures();
		for(int j = 0; j < Constants.dimensions; j++)
		{
			rating += centroidVector[j] * userVector[j];
		}

		if(rating > 0)//Attempt to insert top-k users
		{
			//Not movie ID actually - user is what is added
			users.add(user.getMovieID());
		}

		//Now (all but the first) U will arrive
		while(iter.hasNext())
		{

			user = (NetflixFeatureVector) iter.next();
			//System.out.println("Checking out "+movie.getMovieID());
			rating = 0;
			userVector = user.getFeatures();
				for(int i = 0; i < Constants.dimensions; i++)
				{
					rating += centroidVector[i] * userVector[i];
				}

				if(rating > 0)//Attempt to insert top-k users
				{
					users.add(user.getMovieID());
				}

		}
		
		for(Integer mov : movies)
		{
			StringBuffer buf = new StringBuffer(); 
			buf.append(users.toString());
			context.write(new Text(mov+""), new Text(buf.substring(1,buf.length()-1)));
		}
	}

}

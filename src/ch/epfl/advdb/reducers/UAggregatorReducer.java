package ch.epfl.advdb.reducers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.values.UVMField;

public class UAggregatorReducer extends Reducer<Text,UVMField,Text,Text> {

	float grades[] = new float[Constants.dimensions];
	public void reduce(Text key, Iterable<UVMField> values, Context context) throws IOException, InterruptedException {

		for(UVMField val : values)
		{
			//System.out.println(val.toString());
			int col = val.getCol();
			grades[col - 1] = val.getGrade();
		}

		StringBuffer buf = new StringBuffer();
		int i = 0;
		for(; i < Constants.dimensions - 1; i++)
		{
			buf.append(grades[i]);
			buf.append(',');
		}
		buf.append(grades[i]); //No comma needed for last one

		context.write(new Text("U,"+key.toString()), new Text(buf.toString()));
	}
}
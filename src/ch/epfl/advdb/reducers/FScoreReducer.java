package ch.epfl.advdb.reducers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import ch.epfl.advdb.keys.Movie;
import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.values.UVMField;

public class FScoreReducer extends Reducer<Movie,Text,Text,Text> {

	public void reduce(Movie key, Iterable<Text> values, Context context) throws IOException, InterruptedException {

		int grades[] = new int[Constants.utilMatrixUsers];
		BitSet allUsers = new BitSet();
		BitSet suggestions = new BitSet();

		int uCount = 0; //debugging only
		StringTokenizer itr;
		//Will (+MUST) only iterate twice
		for(Text val : values)
		{
			itr = new StringTokenizer(val.toString()," \t,");
			if(key.getMatrixType() == 'M')
			{
				while(itr.hasMoreTokens())
				{
					int position = Integer.parseInt(itr.nextToken());
					allUsers.set(position);
					grades[position - 1] = Integer.parseInt(itr.nextToken());//1 if relevant / -1 if not
					
				}
			}
			else//X
			{
				while(itr.hasMoreTokens())
				{
					suggestions.set(Integer.parseInt(itr.nextToken()));
					uCount++;
				}
			}

			//System.out.println(key.toString()+ " "+ val.toString());
		}
		//System.out.println("Movie "+key.getMid()+" naively associated with "+uCount+" users");
		System.out.println("M entry contained "+allUsers.cardinality()+" ratings");
		BitSet retrieved = (BitSet) allUsers.clone();
		retrieved.and(suggestions);
		
		for(int i = 0; i < Constants.utilMatrixUsers; i++)
		{
			if(grades[i] <= 0)
			{
				allUsers.clear(i + 1);
			}
		}
		BitSet relevant = (BitSet) allUsers.clone();
						
		BitSet numerator = (BitSet) relevant.clone();
		numerator.and(retrieved); 

		
		float precision = numerator.cardinality() / (float) retrieved.cardinality();
		float recall = numerator.cardinality() / (float) relevant.cardinality();

		float fscore = 1 / ( (1 / recall) + (1 /precision));
		System.out.println("Relevant docs: "+relevant.cardinality()+" - Retrieved: "+retrieved.cardinality());
		System.out.println("Movie "+key.getMid()+"--> Precision: "+precision+" - Recall: "+recall+" - Fscore: "+fscore);
		context.getCounter(Constants.KMeansCounter.fscoreAll).increment((long) (fscore * 10E8));
		if(numerator.cardinality() > 0)
		{
			context.getCounter(Constants.KMeansCounter.nonZero).increment(1);
		}
	}
}
package ch.epfl.advdb.reducers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.values.Feature;
import ch.epfl.advdb.values.NetflixFeatureVector;


/**
 * 
 * @author manolee
 * 
 */
public class NetflixReducer extends Reducer<IntWritable,NetflixFeatureVector,IntWritable,Text> {

	public void reduce(IntWritable key, Iterable<NetflixFeatureVector> values, Context context) throws IOException, InterruptedException {

		float[] allFeatures = new float[Constants.dimensions];

		List<float[]> infoForRadius = new ArrayList<float[]>();


		int itemsInCluster = 0;

		//Get centroid info
		for(NetflixFeatureVector item : values)
		{
			infoForRadius.add(item.getFeatures().clone());

			for(int i = 0; i < Constants.dimensions; i++)
			{
				allFeatures[i] += item.getFeatures()[i];
			}
			itemsInCluster++;
		}

		//Time to writeout new cluster centroid
		StringBuffer value = new StringBuffer();
		int i = 0;
		for(; i < Constants.dimensions - 1; i++ )
		{
			value.append(allFeatures[i] / (float) itemsInCluster );
			value.append(',');
		}
		value.append(allFeatures[i] / (float) itemsInCluster);

		//testing stopping condition
		context.getCounter(Constants.KMeansCounter.netflixStop).increment(1);
		//System.out.println("New centroid: "+value.substring(0,value.length() - 1).toString());
		context.write(key, new Text(value.toString()));

		//Radius computation used during centroid number experiments - no longer needed
		//		double maxRadius = 0;
		//		double currRadius = 0;
		//		for(float[] point : infoForRadius)
		//		{
		//			currRadius = 0;
		//			int pos = 0;
		//			for(float coordVal : point)
		//			{
		//				allFeatures[pos++] -= coordVal;
		//			}
		//			
		//			for(int j = 0 ; j < Constants.dimensions; j++)
		//			{
		//				currRadius += (allFeatures[j] * allFeatures[j]);
		//			}
		//			currRadius = Math.sqrt(currRadius);
		//			maxRadius = currRadius > maxRadius ? currRadius : maxRadius;
		//		}
		//		context.getCounter(Constants.KMeansCounter.netflixRadius).increment((long) (maxRadius * 10E8));
	}
}
package ch.epfl.advdb.reducers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import ch.epfl.advdb.util.Constants;
import ch.epfl.advdb.values.UVMField;

/**
 * 
 * @author manolee
 * Used as part of F-score computation only
 */
public class MAggregatorReducer extends Reducer<Text,UVMField,Text,Text> {

	public void reduce(Text key, Iterable<UVMField> values, Context context) throws IOException, InterruptedException {

		StringBuffer buf = new StringBuffer();
		int flag = 1;//1 if value greater than zero / -1 otherwise. No need to send actual rating
		for(UVMField val : values)
		{
			//System.out.println(val.toString());
			int row = val.getRow();
			float grade = val.getGrade();
			flag = grade > 0 ? 1 : -1;
			buf.append(row);
			buf.append(',');
			buf.append(flag);
			buf.append(',');
		}

		String result = buf.substring(0,buf.length() - 1);

		context.write(new Text("M,"+key.toString()), new Text(result));
	}
}
package ch.epfl.advdb.values;

import ch.epfl.advdb.util.Constants;

public class NetflixCentroid {

	int centroidNo;

	public float[] featureVector;

	public int getCentroidNo() {
		return centroidNo;
	}

	public void setCentroidNo(int centroidNo) {
		this.centroidNo = centroidNo;
	}

	public float[] getFeatureVector() {
		return featureVector;
	}

	public void setFeatureVector(float[] features) {
		this.featureVector = features;
	}

	public NetflixCentroid() {
		super();
		featureVector = new float[Constants.dimensions];
	}

	public NetflixCentroid(int centroidNo, float[] features) {
		super();
		this.centroidNo = centroidNo;
		this.featureVector = features;
	}
}
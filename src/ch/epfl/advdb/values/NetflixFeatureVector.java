package ch.epfl.advdb.values;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.hadoop.io.Writable;

import ch.epfl.advdb.util.Constants;


public class NetflixFeatureVector implements Writable{

	int movieID;//FIXME Do we need it???
	float[] features = new float[Constants.dimensions];
	

	
	public NetflixFeatureVector(int movieID, float[] features) {
		super();
		this.movieID = movieID;
		this.features = features;
	}

	public NetflixFeatureVector() {
	}

	public int getMovieID() {
		return movieID;
	}

	public void setMovieID(int movieID) {
		this.movieID = movieID;
	}

	public float[] getFeatures() {
		return features;
	}

	public void setFeatures(float[] features) {
		this.features = features;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		movieID = in.readInt();
		for(int i = 0 ; i < Constants.dimensions; i++)
		{
			this.features[i] = in.readFloat();
			
		}
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(movieID);
		
		for(int i = 0; i < Constants.dimensions; i++)
		{
			out.writeFloat(this.features[i]);
		}
	}

	@Override
	public String toString() {
		return "NetflixFeatureVector [movieID=" + movieID + ", features="
				+ Arrays.toString(features) + "]";
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	
	
	
	
}

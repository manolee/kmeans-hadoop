package ch.epfl.advdb.values;

/**
 * 
 * @author manolee
 * Used for clusters - assuming they are quite sparse so keeping feature value and position in 'feature vector'
 */
public class Feature {

	int position;
	float value;
	
	public Feature() {
		super();
	
	}

	public Feature(int position, float value) {
		super();
		this.position = position;
		this.value = value;
	}
	
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public float getValue() {
		return value;
	}
	public void setValue(float value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "Feature [position=" + position + ", value=" + value + "]";
	}
	
}

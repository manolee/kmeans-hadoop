package ch.epfl.advdb.values;

import java.util.List;

public class IMDBCentroid {

	int centroidNo;
	
	List<Feature> featureVector;
	
	
	public int getCentroidNo() {
		return centroidNo;
	}
	public void setCentroidNo(int centroidNo) {
		this.centroidNo = centroidNo;
	}
	public List<Feature> getFeatureVector() {
		return featureVector;
	}
	public void setFeatureVector(List<Feature> featureVector) {
		this.featureVector = featureVector;
	}
	
	public IMDBCentroid(int centroidNo, List<Feature> featureVector) {
		super();
		this.centroidNo = centroidNo;
		this.featureVector = featureVector;
	}
	
	public IMDBCentroid() {
		super();
	}
	
}

package ch.epfl.advdb.values;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

/**
 * 
 * @author manolee
 * Used to reduce data sent over the network. Only carries information needed
 * that is not included in actual URefineKey / VRefineKey that it will be 
 * associated with
 */

public class LightValue implements Writable
{
	int complementary; //colNo in the case of URefine, rowNo in the case of VRefine
	float grade;

	public LightValue(char mtype, int complementary, float grade) {
		this.grade = grade;
		this.complementary = complementary;
	}

	public LightValue() {}

	public float getGrade() {
		return grade;
	}



	public void setGrade(float grade) {
		this.grade = grade;
	}

	/**
	 * 
	 * @return either the row or the column of the field in question, 
	 * depending on which of the two is missing from the associated key
	 */
	public int getComplementary() {
		return complementary;
	}

	public void setComplementary(int complementary) {
		this.complementary = complementary;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		complementary = in.readInt();
		grade = in.readFloat();

	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(complementary);
		out.writeFloat(grade);
	}

	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
//		buf.append(",");
		buf.append(complementary);
		buf.append(",");
		buf.append(grade);
		return buf.toString();

	}

}
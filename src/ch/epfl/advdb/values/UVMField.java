package ch.epfl.advdb.values;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;

/**
 * 
 * @author manolee
 * Internal representation of a matrix field ( U or V or M)
 */

public class UVMField implements Writable
{
	int row;
	int col;
	float grade;

	public UVMField(  int row, int col, float grade) {

		this.row = row;
		this.col = col;
		this.grade = grade;

	}

	public UVMField(  String uid, String mid, String grade) {

		this.row = Integer.parseInt(uid);
		this.col = Integer.parseInt(mid);
		this.grade = Float.parseFloat(grade);

	}

	public UVMField() {}

	public float getGrade() {
		return grade;
	}



	public void setGrade(float grade) {
		this.grade = grade;
	}

	public int getRow() {
		return row;
	}



	public void setRow(int row) {
		this.row = row;
	}



	public int getCol() {
		return col;
	}



	public void setCol(int col) {
		this.col = col;
	}



	@Override
	public void readFields(DataInput in) throws IOException {
		row = in.readInt();
		col = in.readInt();
		grade = in.readFloat();

	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(row);
		out.writeInt(col);
		out.writeFloat(grade);
	}

	@Override
	public String toString() {
		StringBuffer buf = new StringBuffer();
		//buf.append(",");
		buf.append(row);
		buf.append(",");
		buf.append(col);
		buf.append(",");
		buf.append(grade);
		return buf.toString();

	}

}
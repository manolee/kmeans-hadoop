package ch.epfl.advdb.values;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.List;

import org.apache.hadoop.io.Writable;


public class IMDBFeatureVector implements Writable {

	int id;
	int featuresNo;
	Integer[] features;
	
	public IMDBFeatureVector() {}
	
	public IMDBFeatureVector(int id, int featuresNo, List<Integer> features) {
		super();
		this.id = id;
		this.featuresNo = featuresNo;
		this.features = features.toArray(new Integer[0]);
		//System.out.println("features?");
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFeaturesNo() {
		return featuresNo;
	}

	public void setFeaturesNo(int featuresNo) {
		this.featuresNo = featuresNo;
	}

	public Integer[] getFeatures() {
		return features;
	}

	public void setFeatures(Integer[] features) {
		this.features = features;
	}

	@Override
	public void readFields(DataInput in) throws IOException {
		id = in.readInt();
		featuresNo = in.readInt();
		this.features = new Integer[featuresNo];
		for(int i = 0; i < featuresNo; i++)
		{
			features[i] = in.readInt();
		}
	}

	@Override
	public void write(DataOutput out) throws IOException {
		out.writeInt(id);
		out.writeInt(featuresNo);
		for(int i = 0; i < featuresNo; i++)
		{
			out.writeInt(features[i]);
		}
	}

	
	
}
